import { MongoClient } from 'mongodb';
import { ADDRGETNETWORKPARAMS } from 'dns';

const url = 'mongodb://localhost:27017/testedb';

async function main() {
    try {
        const cliente = await MongoClient.connect(url);
        console.log('Conectado com sucesso');

        const banco = cliente.db('testebd');
        const colecao = banco.collection('testecol');
        /*
        const umaPessoa = {
            nome: "kelly",
            idade: 23
        };
        const resultIns = await colecao.insertOne(umaPessoa);
        console.log(`Inserido: ${resultIns.insertedId}`);
        */
        //const cursor = colecao.find({});
        /*
        const cursor = colecao.find({ idade: { $gt: 20 } }); // busca idade maior que 25
        console.log(`Contagem: ${await cursor.count()}`);

        //await cursor.forEach(doc => console.log(doc));
        */
        /*
        const resultAlt = await colecao.updateOne({nome: 'Maria'}, {$set: {idade: 45}});
        console.log(`Alteração: ${resultAlt.modifiedCount}`);
        */

        const resultDel = await colecao.deleteOne({nome: 'ana'});
        console.log(`Remoção: ${resultDel.deletedCount}`);

        await cliente.close(); // close encerra conexão
    } catch (erro) {
        console.log('Erro de acesso ao BD:');
        console.log(erro);
    }
}

main();